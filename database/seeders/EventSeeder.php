<?php

namespace Database\Seeders;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;

class EventSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('appointments')->insert([
            'title' => 'Kovács Géza',
            'start' => '2022-06-08 08:00:00',
            'end' => '2022-06-08 10:00:00'
        ]);

        DB::table('appointments')->insert([
            'title' => 'Páros Elemér',
            'start' => '2022-01-01',
            'end' => '2023-12-31',
            'startTime' => '10:00:00',
            'endTime' => '12:00:00',
            'allDay' => false,
            'dow' => '[1]'
        ]);

        DB::table('appointments')->insert([
            'title' => 'Páratlan Dezső',
            'start' => '2022-01-01',
            'end' => '2023-12-31',
            'startTime' => '12:00:00',
            'endTime' => '16:00:00',
            'allDay' => false,
            'dow' => '[3]'
        ]);

        DB::table('appointments')->insert([
            'title' => 'Mindigér Ted',
            'start' => '2022-01-01',
            'end' => '2023-12-31',
            'startTime' => '10:00:00',
            'endTime' => '16:00:00',
            'allDay' => false,
            'dow' => '[5]'
        ]);

        DB::table('appointments')->insert([
            'title' => 'Szilveszterné Szilvia',
            'start' => '2022-06-01',
            'end' => '2022-11-30',
            'startTime' => '16:00:00',
            'endTime' => '20:00:00',
            'allDay' => true,
            'dow' => '[4]'
        ]);


       
    }
}
