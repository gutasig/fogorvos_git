<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Appointments;

class FogorvosController extends Controller
{
    public function index(Request $request) {
        return view('fogorvos');
    }

    public function events(Request $request) {
        if($request->start) {
            $data = Appointments::whereDate('start', '>=', $request->start)
                                ->whereDate('end', '<=', $request->end)
                                ->get(["id", "title", "start", "end", "startTime", "endTime", "allDay", "dow"]);
            return response()->json($data);
        } 
    }

    public function action(Request $request) {
        if($request->type == "add") {
            $event = Appointments::create([
                'title' => $request->title,
                'start' => $request->start,
                'end' => $request->end
            ]);

            return response()->json($event);
        }
    }
}
