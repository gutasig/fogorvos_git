<!DOCTYPE html>
<html lang="hu">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="data:image/x-icon;base64,AAABAAEAEBAAAAEACABoBQAAFgAAACgAAAAQAAAAIAAAAAEACAAAAAAAAAEAAAAAAAAAAAAAAAEAAAAAAACBctEA8Oz/ALfM8gB7bOwAf3DmAKi17QC+0PUAb2bJAIJ25gDA0PUA9Pv/AMPN9QCmvPAAp7zwAMDR+ADH0OwAx874AMTU+ADG0/UArrj2AOHq+gDF1/gA4O36AK3D8wCinOsAzNX7AHdj7QC2wfYAaHCgAHhm7QCzyvYATz3OAL+1/wC7xPYAkJbjAH9t5wC6zPMAqqX0AL3J8wCmrfEAprLuALvN9gBnVegAgXDnAL/N9gCruuIAQje9ANrq+wCGhdUAw9T5AJug5gCfnOwAmKrpAHll2QDk7fsAurvuAKeZ7ADVzvkAtMjxAOjx/gCVi9UAucb0AO32+wC5x/cAqqzmAL/K9wB+eOUAvc/0AL7P9AC/zfcAsKb4AMPN9wDc5f8Aw876AKi88gDC0/cAcWTXAKrB7wDH1vcAx9X9AKGm3gDJ1/oAydj9AI6K3AC3w/UAemvsAJaP2QBFPJgAalfnAIJ15gC/zfgAV1+vAKq67QCowO0AxND4AEg3tgDD0/gAxtD4AMfT+ADJ1vgAssD2ALDI8wC3wfAAvcDkAIOd4AC4xvYA0t7+ANDh/gC8xvYAvMn2ALjN+QDAzO0AlJDvAMHL8wCNqOYAp8DuAHFY6wCrwfEAin7nAK3E8QDl7PsAr8TxAK/H8QB1cNkAzeH2AM/d/AC7wPcAkI/kALnK8QAsOW4Au8j0ALnL9ABqWdQA1d/5ALjM9wC+xvcArqvmALvM9wB/eN8AnJbVAMHM9wCmuu8Av8/3AKqu+ADb4f8ArLD1AIR26wDB0vcArrLyANzm/ABaTsYAqL3vAMLS9wCqwO8Aw9P6AMzW6ACEhuUAzNfrAMbW+gDi6v8Atbb1AMjW+gCMeu4AucTgAHRs4ACvxfUA0NbxALbA7wDO1voAtMTyALHI9QB4bt0AucX1AKaj8wC+x/IAxLb+AK6e9gC7y/UAgHvXAL3L9QDY4P0Av8v1AL3O9QC/zPgAsazqAL/P+ACCb/IAwdH1AKe98ADG0PIA+fz/AEY6uQCyuvAAydT1AEw9uQCux/MAt7r5AL2y/AC6xPMAfmbqALvB/ACUiOwA1N3+AI+V5gC8zfMA2eP1ANjg/gCqrPQAgW/zAMHM+QDDz/kAiYTPADk4fwDF1fkAsrzuAIt76gBmX6oAjHvqAMnY+QCwx/QAemLoALXJ6ACzyvQAt8r0AO71/gC6yvQAvMX3AL3D+gC8yvQAus30AMDN9ACpueYAv9D0AMLO9wCbifkAwNP9AJue8ABaZrcAlq3nAOPu/ACQgtwA5e78AIx98QDQ0voAs8XvAFlswACyxvIAt8LvALPJ8gB7ctoAucbyALfJ8gAAAAAAAAAAAAAAAAAAAAAA6Lss5Pqn972uxqxhwIdPzm1mQDc5i7JTMhshPUFsykhb1NgHTITwADgEkhNJjJCoXIGbnaNniAE16tyi7NEQGfQF7S18L2/zPBpVsCDPXtKO+yJ7jSg2eIW0WANGxcS3S7b2f4rLVMzgFFBf0NnyRWAGBpq4q6TBvvEWnyq6x8ihDhEJtTN2Qo9HPu/CdK+gUjFj1bFprclZZArN0x0rfg28NFHhuVp+JSMnOw8fCAsXqqX1SikSYuLX+d2mLnDmZdvDdXJoiZOC4xgwHJaURJlNd96Gl+62tibWVle/6WtzeoBOnutuDJhDJKmRnHFdefgVldpqfeWD3wI6sz/nHgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=" rel="icon" type="image/x-icon" />
        <title>Fogorvos időpontfoglaló</title>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/fullcalendar@5.11.0/main.min.css" crossorigin="anonymous">
        <link href='https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css' rel='stylesheet'>
        <link href='https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.1/font/bootstrap-icons.css' rel='stylesheet'>
    </head>
    <body>
        <div class="container">
            <br/>
            <h1>Fogorvos időpontfoglaló</h1>
            <br/>

            <div id="calendar"></div>

        </div>

        <script src="https://code.jquery.com/jquery-3.6.0.min.js" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.3/moment.min.js"  crossorigin="anonymous" referrerpolicy="no-referrer"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.3/locale/hu.min.js"  crossorigin="anonymous" referrerpolicy="no-referrer"></script>
        <script src="https://cdn.jsdelivr.net/npm/fullcalendar@5.11.0/main.min.js" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/fullcalendar@5.11.0/locales/hu.js" crossorigin="anonymous"></script>

        <script>
            document.addEventListener('DOMContentLoaded', function() {
                var calendarEl = document.getElementById('calendar');
                var calendar = new FullCalendar.Calendar(calendarEl, {
                    weekText: "Hét: ",
                    weekNumbers: true,
                    initialView: 'dayGridMonth',
                    locale: "hu",
                    editable: true,
                    durationEditable: true,
                    resourceEditable: true,
                    headerToolbar: {
                        start: 'prev,next today', 
                        center: 'title',
                        end: 'dayGridMonth,timeGridWeek,timeGridDay' 
                    },
                    events: "/events",
                    themeSystem: 'bootstrap5',
                    selectable: true,
                    unselectAuto: true,
                    select: function(info) {
                        var title = prompt("Páciens neve:");
                        if(title) {
                            var start = info.startStr;
                            var end = info.endStr;

                            $.ajax({
                                url: "/action",
                                type: "POST",
                                data: {
                                    "_token": "{{ csrf_token() }}",
                                    "title": title,
                                    "start": start,
                                    "end": end,
                                    "type": "add"
                                },
                                success: function(data) {
                                    alert(title+" időpont foglalása sikeresen megtörtént!");
                                    calendar.refetchEvents();
                                },
                                error: function(data) {
                                    alert(title+" időpont foglalása sikertelen!");
                                    calendar.refetchEvents();
                                }
                            });
                        }
                    }
                    
                   
                });
                calendar.render();
            });
        </script>


    </body>
</html>
