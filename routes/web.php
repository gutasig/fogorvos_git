<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\FogorvosController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [FogorvosController::class, 'index']);
Route::get('/events', [FogorvosController::class, 'events']);
Route::post('/action', [FogorvosController::class, 'action']);
